﻿Shader "Sonar/SonarShader"
{
	Properties
	{
		_MainTex("ScreenTex", 2D) = "white" {}
		_Speed("_Speed", Float) = 1.0
		_Pow("_Pow", Float) = 1.0
		_Color("_Color", Color) = (0,0,0,1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;

			float4x4 _ViewInverse;
			float _Speed;
			float _Pow;
			fixed4 _Color;
			float3 _Pos;
			float _Elapsed;
			float _Strength;

			sampler2D _CameraDepthTexture;

			float3 GetFragmentPos(float2 uvCoord)
			{
				float depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uvCoord));
				float2 projectionMultipliers = float2(unity_CameraProjection._11, unity_CameraProjection._22);
				float3 vpos = float3((uvCoord * 2 - 1) / projectionMultipliers, -1)	* depth;
				float4 wsPos = mul(_ViewInverse, float4(vpos, 1));
				return wsPos.xyz;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				float dist = distance(GetFragmentPos(i.uv), _Pos);
				if (dist < _Elapsed * _Speed)
				{
					float str = _Strength * (1 / pow(_Elapsed * _Speed - dist, _Pow));
					col += fixed4(str, str, str, 1) * _Color;
				}
				
				return col;
			}
        ENDCG
        }
    }
}
