﻿using UnityEngine;

public class ScoreController : MonoBehaviour
{
	[System.Serializable]
	private class PlayerScore
	{
		public PlayerNr nr;
		public string name;
		public int score;
	}

	[SerializeField]
	private PlayerScore[] players = new PlayerScore[]
	{
		new PlayerScore {nr = PlayerNr.One, name = "p1", score = 0},
		new PlayerScore {nr = PlayerNr.Two, name = "p2", score = 0}
	};


	public void AddPoint(PlayerNr player)
	{
		foreach (var p in players)
		{
			if (p.nr == player)
			{
				p.score += 1;
			}
		}
	}

	public void ResetScore()
	{
		foreach (var p in players)
		{
			p.score = 0;
		}
	}

	public string GetPlayerScoreString(PlayerNr player)
	{
		foreach (var p in players)
		{
			if (p.nr != player)
			{
				continue;
			}
			return p.name + ": " + p.score.ToString();
		}
		Debug.Assert(false);
		return "";
	}

	public override string ToString()
	{
		var p1 = players[0];
		var p2 = players[1];
		var p1s = GetPlayerScoreString(PlayerNr.One);
		var p2s = GetPlayerScoreString(PlayerNr.Two);
		var ret = p1.score >= p2.score ? p1s : p2s;
		ret += "\n";
		ret += p1.score >= p2.score ? p2s : p1s;
		return ret;
	}
}