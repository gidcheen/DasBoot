﻿using System.Collections;
using UnityEngine;

public class MenuSonarEffect : MonoBehaviour
{
	[SerializeField]
	private float timeBetweenSonar = 2;
	[SerializeField]
	private Transform sub1;
	[SerializeField]
	private Transform sub2;

	private IEnumerator Sonar1()
	{
		while (true)
		{
			SonarPostProcess.AddWave(sub1.position);
			yield return new WaitForSeconds(timeBetweenSonar);
			SonarPostProcess.AddWave(sub2.position);
			yield return new WaitForSeconds(timeBetweenSonar);
		}
	}

	private void Start()
	{
		StartCoroutine(Sonar1());
	}

	private void OnDestroy()
	{
		SonarPostProcess.ClearAllWaves();
	}
}