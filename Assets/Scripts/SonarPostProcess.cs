﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class SonarPostProcess : MonoBehaviour
{
	private Camera cam;
	[SerializeField]
	private Material mat;
	[SerializeField]
	private Material deathMat;
	private static Object bingSound;

	private const float WaveDuration = 9;

	private int viewInverseId;
	private int posId;
	private int elapsedId;
	private int strengthId;

	private class WaveData
	{
		public Vector3 pos;
		public float elapsed = 0;
		public bool death = false;
	}

	private static readonly List<WaveData> waves = new List<WaveData>();

	public static void AddWave(Vector3 pos, bool death = false)
	{
		waves.Add(new WaveData {pos = pos, death = death});
		if (bingSound == null)
		{
			bingSound = Resources.Load("BingSound");
		}
		Instantiate(bingSound);
	}

	public static void ClearAllWaves()
	{
		waves.Clear();
	}

	private void Awake()
	{
		viewInverseId = Shader.PropertyToID("_ViewInverse");
		posId = Shader.PropertyToID("_Pos");
		elapsedId = Shader.PropertyToID("_Elapsed");
		strengthId = Shader.PropertyToID("_Strength");
	}

	private void Start()
	{
		cam = GetComponent<Camera>();
		cam.depthTextureMode = DepthTextureMode.Depth;
	}

	private void Update()
	{
		for (var i = waves.Count - 1; i >= 0; i--)
		{
			var w = waves[i];
			w.elapsed += Time.deltaTime;
			if (w.elapsed > WaveDuration)
			{
				waves.Remove(w);
			}
		}
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		mat.SetMatrix(viewInverseId, cam.worldToCameraMatrix.inverse);
		deathMat.SetMatrix(viewInverseId, cam.worldToCameraMatrix.inverse);
		foreach (var w in waves)
		{
			var m = w.death ? deathMat : mat;
			m.SetVector(posId, w.pos);
			m.SetFloat(elapsedId, w.elapsed);
			m.SetFloat(strengthId, Mathf.Pow(1 - w.elapsed / WaveDuration, 0.9f));
			Graphics.Blit(source, source, m);
		}
		Graphics.Blit(source, destination);
	}
}