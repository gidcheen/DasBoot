﻿using UnityEngine;

public class TorpedoController : ContactDestroy
{
	[SerializeField]
	private float speed = 3;

	private void Start()
	{
		GetComponent<Rigidbody>().velocity = transform.forward * speed;
	}

	private void GameOver()
	{
		GetComponent<Rigidbody>().velocity = new Vector3();
		GetComponent<Rigidbody>().angularVelocity = new Vector3();
		GetComponent<Rigidbody>().isKinematic = true;
		GetComponentInChildren<Collider>().enabled = false;
	}
}
