﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
	[SerializeField]
	public Transform spawnPointsRoot;
	[SerializeField]
	private Transform spawnParent;
	[SerializeField]
	private Object toSpawn;
	[SerializeField]
	private int minSpawns;
	[SerializeField]
	private int maxSpawns;
	[SerializeField]
	private bool randomRot;
	[SerializeField]
	private bool spawnOnStart;

	private void Start()
	{
		if (Application.isPlaying)
		{
			UnspawnBounds();
			if (spawnOnStart)
			{
				Spawn();
			}
		}
		else
		{
			SpawnBounds();
		}
	}

	private void OnDestroy()
	{
		if (Application.isPlaying)
		{
			return;
		}
		UnspawnBounds();
	}

	public void Spawn()
	{
		Debug.Assert(spawnPointsRoot.childCount >= maxSpawns);
		var numToSpawn = Random.Range(minSpawns, maxSpawns + 1);
		var spawnPoints = spawnPointsRoot.Cast<Transform>().ToList();
		for (var i = 0; i < numToSpawn; i++)
		{
			var spawnPos = spawnPoints[Random.Range(0, spawnPoints.Count)];
			SpawnAt(spawnPos);
			spawnPoints.Remove(spawnPos);
		}
	}

	private GameObject SpawnAt(Transform t)
	{
		var rot = randomRot
			? Quaternion.Euler(t.rotation.eulerAngles.x, Random.Range(0, 360), t.rotation.eulerAngles.z)
			: t.rotation;
		return Instantiate(toSpawn, t.position, rot, spawnParent) as GameObject;
	}

	public void SpawnBounds()
	{
		UnspawnBounds();
		var instance = (Instantiate(toSpawn) as GameObject);
		Bounds? bounds = null;
		foreach (var col in instance.GetComponentsInChildren<Renderer>())
		{
			if (bounds == null)
			{
				bounds = col.bounds;
			}
			else
			{
				bounds.Value.Encapsulate(col.bounds);
			}
		}
		foreach (Transform c in spawnPointsRoot)
		{
			var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			sphere.transform.parent = c;
			sphere.transform.localPosition = new Vector3();
			sphere.transform.localScale = new Vector3(1, 1, 1) * bounds.Value.size.magnitude;
		}
		DestroyImmediate(instance);
	}

	public void UnspawnBounds()
	{
		foreach (Transform t in spawnPointsRoot)
		{
			if (t.childCount == 0)
			{
				continue;
			}
			DestroyImmediate(t.GetChild(0).gameObject);
		}
	}
}