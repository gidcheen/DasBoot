﻿using UnityEngine;

public class RandomizePitch : MonoBehaviour
{
	private void Start()
	{
		GetComponent<AudioSource>().pitch = Random.Range(0.98f, 1.02f);
	}
}