﻿using System.Collections;
using UnityEngine;

public class MazeGoal : MonoBehaviour
{
	private static IEnumerator Blink(Vector3 point)
	{
		while (true)
		{
			SonarPostProcess.AddWave(point);
			yield return new WaitForSeconds(4);
		}
	}

	// Use this for initialization
	private void Start()
	{
		StartCoroutine(Blink(transform.position));
	}
}