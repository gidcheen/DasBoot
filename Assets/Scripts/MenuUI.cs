﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUI : MonoBehaviour
{
	private void Awake()
	{
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	public void LoadGameMode(string gameMode)
	{
		SceneManager.LoadScene("Level");
		SceneManager.LoadScene(gameMode, LoadSceneMode.Additive);
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void ResetScoreClick()
	{
		GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreController>().ResetScore();
		GameObject.Find("ScoreText").GetComponent<DisplayScore>().SetScore();
	}
}
