﻿using System.Collections;
using UnityEngine;

public class CaptureTheBase : GameMode
{
	[SerializeField]
	private SpawnController respawner;
	[SerializeField]
	private float timeTillRespawn = 1;
	[SerializeField]
	private float blinks = 1;

	protected override void PlayerDead(PlayerController player)
	{
		base.PlayerDead(player);

		StartCoroutine(DelayedRespawn(player));
		StartCoroutine(RespawnBlink(player.transform.position));

		player.SetColor(Color.red);

		MessageRoot.Message("Respawn", player);
	}

	private void GoalHit(PlayerController player)
	{
		StartCoroutine(DeathBlink(player.transform.position));

		var score = GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreController>();
		score.AddPoint(player.PlayerNr == PlayerNr.One ? PlayerNr.One : PlayerNr.Two);

		player.SetColor(Color.green);

		MessageRoot.Message("GameOver");
	}

	private IEnumerator RespawnBlink(Vector3 point)
	{
		for (var i = 0; i < blinks; i++)
		{
			SonarPostProcess.AddWave(point, true);
			yield return new WaitForSeconds(timeTillRespawn / (float)blinks);
		}
		SonarPostProcess.AddWave(point, true);
	}

	private IEnumerator DelayedRespawn(PlayerController player)
	{
		yield return new WaitForSeconds(timeTillRespawn);
		Destroy(player.gameObject);
		yield return new WaitForSeconds(0.1f);
		respawner.Spawn();
	}
}
