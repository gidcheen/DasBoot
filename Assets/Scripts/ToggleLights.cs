﻿using UnityEngine;

public class ToggleLights : MonoBehaviour
{
	[SerializeField]
	private bool startOff;

	private GameObject b;

	private void Start()
	{
		b = transform.GetChild(0).gameObject;
		b.SetActive(!startOff);
	}

	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			b.SetActive(!b.activeInHierarchy);
		}
	}
}
