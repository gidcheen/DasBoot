﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayUI : MonoBehaviour
{
	public void OnToMenuClick()
	{
		SonarPostProcess.ClearAllWaves();
		SceneManager.LoadScene("Menu");
	}
}
