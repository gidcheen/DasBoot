﻿using UnityEngine;

public class MineController : ContactDestroy
{
	private Animator animator;

	private void Start()
	{
		animator = GetComponent<Animator>();
		var state = animator.GetCurrentAnimatorStateInfo(0);
		animator.Play(state.fullPathHash, -1, Random.Range(0f, 1f));
	}

	private void GameOver()
	{
		animator.speed = 0;
	}
}
