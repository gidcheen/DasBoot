﻿using UnityEngine;

public class MessageRoot : MonoBehaviour
{
	public static void Message(string msg, object parameter = null)
	{
		foreach (var r in GameObject.FindGameObjectsWithTag("Root"))
		{
			if (parameter == null)
				r.BroadcastMessage(msg, SendMessageOptions.DontRequireReceiver);
			else
				r.BroadcastMessage(msg, parameter, SendMessageOptions.DontRequireReceiver);
		}
	}
}
