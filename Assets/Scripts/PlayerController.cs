﻿using System.Collections;
using UnityEngine;

public enum PlayerNr
{
	None,
	One,
	Two
};

public class PlayerController : MonoBehaviour
{
	public PlayerNr PlayerNr { get; private set; }

	[SerializeField]
	private float pitchForce = 1;
	[SerializeField]
	private float yawForce = 1;
	[SerializeField]
	private float thrustForce = 1;
	[SerializeField]
	private float torpedoWaitTime = 1;

	[SerializeField]
	private Object torpedo;

	private Camera cam;
	private Rigidbody rb;

	private Transform centerOfGravity;
	private Transform finPos1;
	private Transform finPos2;
	private Transform torpedoStart;

	private string nrStr;

	private bool canFire = true;
	private bool canMove = true;
	private static readonly int EmissionColor = Shader.PropertyToID("_EmissionColor");

	private struct InputNames
	{
		public const string Pitch = "Pitch";
		public const string Yaw = "Yaw";
		public const string Thrust = "Thrust";
		public const string Shoot = "Shoot";
		public const string Sonar = "Sonar";
	}

	private IEnumerator ReEnableFire()
	{
		yield return new WaitForSeconds(torpedoWaitTime);
		canFire = true;
	}

	private void Freeze()
	{
		rb.velocity = new Vector3();
		rb.angularVelocity = new Vector3();
		canMove = false;

		foreach (var col in GetComponentsInChildren<Collider>())
		{
			col.enabled = false;
		}
	}

	private void GameOver()
	{
		Freeze();

		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	private void Respawn(PlayerController player)
	{
		if (player == this) Freeze();
	}

	public void SetColor(Color color)
	{
		foreach (var rend in GetComponentsInChildren<Renderer>())
		{
			rend.material.SetColor(EmissionColor, color);
			rend.material.EnableKeyword("_EMISSION");
		}
	}

	private void Awake()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

		foreach (var p in GameObject.FindGameObjectsWithTag("Player"))
		{
			if (p == gameObject)
			{
				continue;
			}
			var otherPc = p.GetComponent<PlayerController>();
			if (otherPc.PlayerNr == PlayerNr.None || otherPc.PlayerNr == PlayerNr.Two)
			{
				PlayerNr = PlayerNr.One;
			}
			else
			{
				PlayerNr = PlayerNr.Two;
			}
		}
	}

	private void Start()
	{
		cam = gameObject.GetComponentInChildren<Camera>();
		var r = cam.rect;
		r.x = PlayerNr == PlayerNr.One ? 0 : 0.51f;
		cam.rect = r;

		rb = GetComponent<Rigidbody>();
		rb.constraints = RigidbodyConstraints.FreezeRotationZ;

		nrStr = PlayerNr == PlayerNr.One ? "1" : "2";

		centerOfGravity = transform.Find("CenterOfGravity");
		rb.centerOfMass = centerOfGravity.localPosition;

		finPos1 = transform.Find("FinPos1");
		finPos2 = transform.Find("FinPos2");
		torpedoStart = transform.Find("TorpedoStart");
	}

	private void Update()
	{
		if (!canMove)
		{
			return;
		}
		if (Input.GetButtonDown(InputNames.Sonar + nrStr))
		{
			SonarPostProcess.AddWave(transform.position);
		}
		if (Input.GetButtonDown(InputNames.Shoot + nrStr) && canFire)
		{
			Instantiate(torpedo, torpedoStart.position, torpedoStart.rotation,
				GameObject.FindGameObjectWithTag("Root").transform);
			canFire = false;
			StartCoroutine(ReEnableFire());
		}
	}

	private void FixedUpdate()
	{
		transform.localRotation = Quaternion.Euler(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
		if (!canMove)
		{
			return;
		}

		var pitch = Input.GetAxis(InputNames.Pitch + nrStr);
		rb.AddForceAtPosition(finPos1.up * (pitch * pitchForce), finPos1.position);
		rb.AddForceAtPosition(-finPos2.up * (pitch * pitchForce), finPos2.position);

		var yaw = Input.GetAxis(InputNames.Yaw + nrStr);
		rb.AddForceAtPosition(-finPos1.right * (yaw * yawForce), finPos1.position);
		rb.AddForceAtPosition(finPos2.right * (yaw * yawForce), finPos2.position);

		var thrust = Input.GetAxis(InputNames.Thrust + nrStr);
		rb.AddForceAtPosition(finPos1.forward * (thrust * thrustForce), finPos1.position);
	}

	private void OnCollisionEnter(Collision collision)
	{
		var interactable = collision.gameObject.GetComponent<Interactable>();
		if (interactable == null)
		{
			return;
		}
		MessageRoot.Message(interactable.messageToSend, this);
	}
}