﻿using UnityEngine;

public class DestroyIfDonePlaying : MonoBehaviour
{
	private void Update()
	{
		if(!GetComponent<AudioSource>().isPlaying)
		{
			Destroy(gameObject);
		}
	}
}
