﻿using UnityEngine;

public class DestroyIfExists : MonoBehaviour
{
	private void Start()
	{
		if (GameObject.FindGameObjectsWithTag(gameObject.tag).Length > 1)
		{
			Destroy(gameObject);
		}
	}
}
