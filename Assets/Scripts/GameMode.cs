﻿using System.Collections;
using UnityEngine;

public abstract class GameMode : MonoBehaviour
{
	protected virtual void PlayerDead(PlayerController player) { }

	protected static IEnumerator DeathBlink(Vector3 point)
	{
		while (true)
		{
			SonarPostProcess.AddWave(point, true);
			yield return new WaitForSeconds(4);
		}
	}
}
