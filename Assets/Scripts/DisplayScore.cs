﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour
{
	[SerializeField]
	private PlayerNr playerNr = PlayerNr.One;
	[SerializeField]
	private bool allPlayers;

	private void Start()
	{
		SetScore();
	}

	public void SetScore()
	{
		var text = GetComponent<Text>();
		var score = GameObject.Find("Score").GetComponent<ScoreController>();
		text.text = allPlayers ? score.ToString() : score.GetPlayerScoreString(playerNr);
	}

	private void GameOver()
	{
		SetScore();
	}
}
