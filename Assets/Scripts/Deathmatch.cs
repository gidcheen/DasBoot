﻿using UnityEngine;

public class Deathmatch : GameMode
{
	protected override void PlayerDead(PlayerController player)
	{
		base.PlayerDead(player);

		var score = GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreController>();
		score.AddPoint(player.PlayerNr == PlayerNr.One ? PlayerNr.Two : PlayerNr.One);

		player.SetColor(Color.red);
		StartCoroutine(DeathBlink(player.transform.position));

		MessageRoot.Message("GameOver");
	}
}
