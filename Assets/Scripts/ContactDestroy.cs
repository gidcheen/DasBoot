﻿using UnityEngine;

public class ContactDestroy : MonoBehaviour
{
	[SerializeField]
	private Object explosionSound;

	protected virtual void OnCollisionEnter(Collision collision)
	{
		SonarPostProcess.AddWave(collision.contacts[0].point);
		Destroy(gameObject);
		Instantiate(explosionSound);
	}
}
