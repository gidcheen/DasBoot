﻿using UnityEngine;

public class EnableOnGameOver : MonoBehaviour {
	private void Start()
	{
		foreach(Transform c in transform)
		{
			c.gameObject.SetActive(false);
		}
	}

	private void GameOver()
	{
		foreach (Transform c in transform)
		{
			c.gameObject.SetActive(true);
		}
	}
}
