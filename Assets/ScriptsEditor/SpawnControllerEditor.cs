﻿using UnityEditor;
using UnityEngine;

namespace ScriptsEditor
{
	[CustomEditor(typeof(SpawnController))]
	[CanEditMultipleObjects]
	public class SpawnControllerEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
			var script = (SpawnController)target;
			if (GUILayout.Toggle(script.spawnPointsRoot.GetChild(0).childCount != 0, "Show Bounds"))
			{
				if (script.spawnPointsRoot.GetChild(0).childCount == 0) script.SpawnBounds();
			}
			else
			{
				if (script.spawnPointsRoot.GetChild(0).childCount != 0) script.UnspawnBounds();
			}
		}
	}
}
